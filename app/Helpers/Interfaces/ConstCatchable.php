<?php

namespace App\Helpers\Interfaces;

interface ConstCatchable {
    //Get all consts
    public static function getAllConsts();
    //Get consts define by array
    public static function getConstByArray(array $keys);
    //Get one consts
    public static function getSpecifiedConst($name);
}

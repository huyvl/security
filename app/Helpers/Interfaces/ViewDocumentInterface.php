<?php
namespace App\Helpers\Interfaces;
interface ViewDocumentInterface {
    public static function word2pdf($dir);
    public static function txt2pdf($dir);
    public static function excel2pdf($dir, $sheetIndex);
    public function convert();
}

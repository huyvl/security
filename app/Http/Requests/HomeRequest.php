<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name' => [
                            'required',
                            'max:255',
                            'unique:users,name,' . null . ',id,status,1',

                        ],
                        'email' => [
                            'required',
                            'unique:users,email,' . null . ',id,status,1',
                            'regex: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
                        ],
                        'image' => 'image|mimes:jpeg,JPEG,png,PNG,jpg,JPG|max:2069',
                    ];
                }
            case 'PATCH':
                {
                    return [
                        'name' => [
                            'required',
                            'max:255',
                            'unique:users,name,' . $id . ',id,status,1',
                        ],
                        'email' => 'required|unique:users,email,' . $id . ',id,status,1|regex: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
                        'image' => 'image|mimes:jpeg,JPEG,png,PNG,jpg,JPG|max:2069',
                    ];
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên đăng nhập.',
            'name.unique' => 'Tên đăng nhập đã tồn tại.',
            'name.max' => 'Họ và tên không vượt quá 255 kí tự',
            'email.required' => 'Vui lòng nhập email',
            'email.regex' => 'Email sai định dạng . Vui lòng nhập email lại',
            'email.unique' => 'Email đã tồn tại trong hệ thống',
            'image.mimes' => 'Ảnh phải thuộc 1 trong các định dạng .PNG, .JPG, .JPEG',
            'image.image' => 'Ảnh phải thuộc 1 trong các định dạng .PNG, .JPG, .JPEG',
            'image.max' => 'Dung lượng ảnh không được vượt quá 2MB',
        ];
    }


}

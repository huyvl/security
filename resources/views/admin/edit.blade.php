@extends('admin.layouts.master')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Edit User
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1"
                          enctype="multipart/form-data"
                          action="{{ route('admin.update', $user->id) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row {{ $errors->has('name') ? 'has-danger' : ''}}">
                                <label class="col-form-label col-lg-3 col-sm-12">Username *</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <div class='input-group'>
                                        <input type='text' class="form-control m-input" name="name"
                                               placeholder="Input username" value="{{ old('name', $user->name) }}"/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group m-form__group row {{ $errors->has('email') ? 'has-danger' : ''}}">
                                <label class="col-form-label col-lg-3 col-sm-12">Email *</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <div class='input-group'>
                                        <input type="email" class="form-control m-input" name="email"
                                               placeholder="Input email" value="{{ old('email', $user->email) }}"/>
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Image</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <div class='input-group'>
                                        <input type='file' class="form-control m-input img-choice" name="image"
                                               accept="image/*"
                                               onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12"></label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <img id="blah" alt="blank-user"
                                         src="{{ get_image($user->image, get_const('USER_UPLOAD')) }}"
                                         class="img-thumbnail img-preview" style="width: 160px;">
                                    <br/>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="{{route('admin.home')}}" class="btn btn-secondary">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

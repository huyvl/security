@extends('admin.layouts.master')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                List User
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <!--begin: Search Form -->
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..."
                                                   id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="la la-search"></i></span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="{{route('admin.create')}}"
                                   class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus"></i>
													<span>Create User</span>
												</span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>

                    <!--end: Search Form -->

                    <!--begin: Datatable -->
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                        <tr>
                            <th title="Field #1" data-field="Image">Image</th>
                            <th title="Field #2" data-field="Username">Username</th>
                            <th title="Field #3" data-field="Email">Email</th>
                            <th title="Field #4" data-field="Action">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="feed-element text-center">
                                    <img alt="image" class="img-circle" width="48px" height="48px"
                                         src="{{ get_image($user->image, get_const('USER_UPLOAD')) }}">
                                </td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="{{ route('admin.delete',['id' => $user->id ]) }}"
                                       class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                        <i class="la la-trash"></i>
                                    </a>
                                    <a href="{{ route('admin.edit',['id' => $user->id ]) }}"
                                       class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                        <i class="la la-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.layouts.message')

    <!--begin::Page Scripts -->
    <script src="{{ asset('public/js/common.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js') }}"
            type="text/javascript">
    </script>
    <!--end::Page Scripts -->
@endsection

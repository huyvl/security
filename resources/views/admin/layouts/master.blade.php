<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
@include('admin.layouts.head')

<!-- end::Head -->

<!-- begin::Body -->
<body
    class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
@include('admin.layouts.header')

<!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
    @include('admin.layouts.left')

    <!-- END: Left Aside -->
        @yield('content')
    </div>

    <!-- end:: Body -->

    <!-- begin::Footer -->
@include('admin.layouts.footer')

<!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Quick Sidebar -->
@include('admin.layouts.sidebar')

<!-- end::Quick Sidebar -->

<!-- begin::Scroll Top -->
@include('admin.layouts.scroll')

<!-- end::Scroll Top -->

<!--begin::Page Scripts -->
@include('admin.layouts.script')

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>

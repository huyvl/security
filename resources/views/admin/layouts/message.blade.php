@if (Session::has('message'))
    <script type="text/javascript">
        swal("Success!", "{{ Session::get('message') }}", "success")
    </script>
@endif
@if (Session::has('error'))
    <script type="text/javascript">
        swal("Error!", "{{ Session::get('error') }}", "error")
    </script>
@endif
@if (Session::has('warning'))
    <script type="text/javascript">
        swal("Warning!", "{{ Session::get('warning') }}", "warning")
    </script>
@endif
@if (Session::has('info'))
    <script type="text/javascript">
        swal("Info!", "{{ Session::get('info') }}", "info")
    </script>
@endif


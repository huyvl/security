<!--begin::Global Theme Bundle -->
<script src="{{ asset('public/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<script src="{{ asset('public/assets/app/js/dashboard.js') }}" type="text/javascript"></script>
<!--begin::Page Scripts -->
<script src="{{ asset('public/assets/demo/default/custom/components/base/sweetalert2.js') }}" type="text/javascript"></script>

<!--end::Page Scripts -->
@yield('script')


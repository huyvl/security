<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16"
          href="{{ asset('public/assets/demo/default/media/img/logo/favicon.ico') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('public/assets/node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- This is for the animation CSS -->
    <link href="{{ asset('public/assets/node_modules/aos/dist/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/node_modules/prism/prism.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}"
          rel="stylesheet">
    <!-- This page CSS -->
    <!-- Custom CSS -->
    <link href="{{ asset('public/css/sliders/static-slider1-10.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="">
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Wrapkit</p>
    </div>
</div>
<div id="main-wrapper">
    <div class="topbar">
        <div class="header6">
            <div class="container po-relative">
                <nav class="navbar navbar-expand-lg h6-nav-bar">
                    <a href="javascript:void(0)" class="navbar-brand"><img
                            src="{{ asset('public/assets/app/media/img/logos/logo-1.png') }}" alt="wrapkit"/></a>
                    <h1 style="color: white">Modsecurity</h1>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h6-info"
                            aria-controls="h6-info" aria-expanded="false" aria-label="Toggle navigation"><span
                            class="ti-menu"></span></button>
                    <div class="collapse navbar-collapse hover-dropdown font-14 ml-auto" id="h6-info">
                        <ul class="navbar-nav ml-auto">
                        </ul>
                        @auth
                            <div class="act-buttons">
                                <a href="{{ route('admin.home') }}" class="btn btn-success-gradiant font-14">Admin</a>
                            </div>
                        @else
                            <div class="act-buttons">
                                <a href="{{ route('login') }}" class="btn btn-success-gradiant font-14">Login</a>
                            </div>
                        @endauth
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="banner-innerpage"
                 style="background-image:url({{ asset('public/assets/images/innerpage/banner-bg.jpg') }})">
            </div>
            <div class="static-slider1">
                <div class="container">
                    <!-- Row  -->
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-7 align-self-center" data-aos="fade-right" data-aos-duration="1200">
                            <h1 class="title">Hello World, <span class="text-success-gradiant typewrite"
                                                                 data-period="2000"
                                                                 data-type='[ "Modsecurity" ]'></span>
                            </h1>
                        </div>
                        <!-- Column -->
                        <div class="col-md-5 img-anim m-t-40" data-aos="fade-up" data-aos-duration="2200">
                            <img src="{{ asset('public/assets/images/sliders/static-slider/slider1/img1.png') }}" alt="wrapkit"
                                 class="img-fluid"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
</div>
<div class="footer4 spacer b-t">
    <div class="container">
        <div class="f4-bottom-bar">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex font-14">
                        <div class="ml-auto m-t-10 m-b-10 copyright">All Rights Reserved by Modsecurity Team.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('public/assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{ asset('public/assets/node_modules/popper/dist/popper.min.js') }}"></script>
<script src="{{ asset('public/assets/node_modules/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- This is for the animation -->
<script src="{{ asset('public/assets/node_modules/aos/dist/aos.js') }}"></script>
<script src="{{ asset('public/assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('public/js/custom.min.js') }}"></script>
<script src="{{ asset('public/assets/node_modules/prism/prism.js') }}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<script src="{{ asset('public/js/type.js') }}"></script>
<script>
    // This is for the static slider 6
    $('.video-img').on('click', function () {
        $(this).addClass('hide');
        $('.embed-responsive').show()
            .removeClass('hide');
        $("video").each(function () {
            this.play()
        });
    });
    $('.pre-scroll').perfectScrollbar();
</script>
</body>

</html>

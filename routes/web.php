<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::group(['middleware' => array('auth')], function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/', 'Admin\HomeController@index')->name('home');
        Route::get('/create', 'Admin\HomeController@create')->name('create');
        Route::post('/store', 'Admin\HomeController@store')->name('store');
        Route::get('/edit/{id?}', 'Admin\HomeController@edit')->name('edit');
        Route::patch('/update/{id?}', 'Admin\HomeController@update')->name('update');
        Route::get('/delete/{id?}', 'Admin\HomeController@destroy')->name('delete');
    });
});


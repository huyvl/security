-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 23, 2018 at 06:28 AM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `security`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `image` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `image`, `password`, `remember_token`, `created_at`, `status`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, NULL, '$2y$10$9nFmQjxp3ItH/o3xcKJgduqKzHmOrtx6WrNPxQXAsd9nwTar.NcFa', 'gAC2WwjeiHqvDXb99rbhH0OppdDFK68OADPw4ZjN5JZeoxLA7SfCQawtzS8S', '2018-12-02 09:10:54', 1, '2018-12-02 09:10:54'),
(7, 'se03654', 'hoannt@gmail.com', NULL, NULL, '$2y$10$NxjRGCVAhdWvIa1qQb9Mye4no11TDo4eOpHLu3XtWWf9.SmLtJEYu', NULL, '2018-12-22 15:41:11', 1, '2018-12-22 15:41:11'),
(8, '<xs:element name=\"transaction\"></xs:element>', 'hoan@gmail.com', NULL, NULL, '$2y$10$cd1zxrTPiOgWSiPqp0W03u2dedQ60F0x29HSdSUyPYR9MBG5cW6xC', NULL, '2018-12-22 18:20:55', 0, '2018-12-22 18:21:03'),
(9, '<user>  	       <uname>joepublic</uname>  	       <pwd>r3g</pwd>  	       <uid>0<uid/> 	       <mail>joepublic@example1.com</mail> 	</user>', 'hoan@gmail.com', NULL, NULL, '$2y$10$pXwwqqQGM3jZjAHXWOAtueFtXfCO6a/FMFZ2fV.Cz.wszF.dZzCZa', NULL, '2018-12-22 18:22:23', 0, '2018-12-22 18:22:28'),
(10, 'aaa', 'hoan@gmail.com', NULL, NULL, '$2y$10$YdEYnsQxyTYIYZUOud6I0OWo4N1zLTs4abb1nUdGkAp180Q7.N6PO', NULL, '2018-12-22 20:19:41', 0, '2018-12-22 20:19:45'),
(11, '<a href=\"http://www.chapelofresonance.com/\"></a>', 'hoan@gmail.com', NULL, NULL, '$2y$10$u3qnD.6buNoYS2A53c2OduKzbbvAYU/A.C6Q.47Ah1PdV4oMdIrc2', NULL, '2018-12-22 20:36:33', 0, '2018-12-22 20:36:52'),
(12, '.!@#', 'qwe@gmail.com', NULL, NULL, '$2y$10$whb5EpiWcN5khDtAGwPnOuufvL7H5f47XTo6TluSAtKJhi2Ypzybm', NULL, '2018-12-23 00:46:48', 0, '2018-12-23 00:46:53'),
(13, 'a e . @', 'hoan@gmail.com', NULL, NULL, '$2y$10$ESOwwFxHKs11yyFHRfbya.JFfH7n.PYzonKicC5euPk.mbpaTpREu', NULL, '2018-12-23 00:55:37', 0, '2018-12-23 00:55:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
